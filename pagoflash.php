<?php
include( plugin_dir_path( __FILE__ ) .'inc/pagoflash.api.client.php');
/* Plugin Name: Pago Flash ISO
  Plugin URI: http://konkiro.org
  Description: Pago flash para wordpress
  Version: 1.0
  Author: Israel Simmons Oliveros (ISO)
  Author URI: http://www.progressumit.com/
  License: GPLv2 or later
 */

function pagoflash_activation() {
    
}

register_activation_hook(__FILE__, 'pagoflash_activation');

function pagoflash_deactivation() {
    
}

register_deactivation_hook(__FILE__, 'pagoflash_deactivation');

if (is_admin()){
    add_action('admin_menu', 'pagoflash_plugin_settings');
}


function pagoflash_plugin_settings() {
    add_menu_page('PagoFlash Settings', 'Configuración PagoFlash', 'administrator', 'pagoflash_settings', 'pagoflash_display_settings');
}

function pagoflash_display_settings() {    
    wp_enqueue_style( 'jquery-ui', plugin_dir_url( __FILE__ ) . 'inc/css/jquery-ui' );
    wp_enqueue_script( 'jquery-ui', plugin_dir_url( __FILE__ ).'inc/js/jquery-ui');
    include WP_PLUGIN_DIR . '/pagoflash/inc/admintemplate.php';
}

function btnPagoFlash($parametros) {
    $p_key_token = get_option('pagoflash_key');
    $p_key_secret = get_option('pagoflash_secret');
    $p_url_punto_venta = get_option('pagoflash_ptoventa');
    $p_modo_prueba = (strtolower(get_option('pagoflash_testmode')) == 'on') ? true : false;
    $apiPF = new apiPagoflash($p_key_token, $p_key_secret, $p_url_punto_venta, $p_modo_prueba);
    $product_1['SITE_URL']=  urlencode($p_url_punto_venta);
    $product_1['AMOUNT']=100;
    $product_1['ITEM_DESC']="Donacion Hogar Bambi";
    $product_1['ITEM_QTY']=1;
    $product_1['ITEM_IMG']="";
    $link= $apiPF->generarURLPago($product_1);
    
    $size = isset($parametros['size']) ? $parametros['size'] : 1;
    switch ($size) {
        case '1':
            $boton = plugins_url('images/pf_144_44.png', __FILE__);
            break;
        case '2':
            $boton = plugins_url('images/pf_274_86.png', __FILE__);
            break;
    }
    $monto = $parametros['monto'];
    
    include( plugin_dir_path( __FILE__ ) .'inc/btnTemplate.php');
    
    $script='<script>jQuery(function(){jQuery("#urldonacion").on("click", function(e){e.preventDefault();jQuery.ajax({dataType:"json",url:jQuery("#urldonacion").attr("href"), type:"post"}).done(function(){alert("ok")})})})</script>';
    //return '<a id="urldonacion" href="'.$link.'"><img src="' . plugins_url('images/' . $boton . '.png', __FILE__) . '" /></a>';
    //return '<a id="urldonacion" href="http://www.nomoremondays.dev/?page_id=25"><img src="' . plugins_url('images/' . $boton . '.png', __FILE__) . '" /></a>';
}

add_shortcode('btnPagoFlash', 'btnPagoFlash');

function frmPagoFlash(){
    include( plugin_dir_path( __FILE__ ) .'inc/frmTemplate.php');
}
add_shortcode('frmPagoFlash', 'frmPagoFlash');

add_action('template_redirect', 'check_for_event_submissions');

function check_for_event_submissions() {
    $p_key_token = get_option('pagoflash_key');
    $p_key_secret = get_option('pagoflash_secret');
    $p_url_punto_venta = get_option('pagoflash_ptoventa');
    $p_modo_prueba = (strtolower(get_option('pagoflash_testmode')) == 'on') ? true : false;
    $apiPF = new apiPagoflash($p_key_token, $p_key_secret, $p_url_punto_venta, $p_modo_prueba);

    $wpnonce=filter_input(INPUT_POST, '_wpnonce');
    if (wp_verify_nonce($wpnonce,'frm_donacion')){
        $dona=filter_input(INPUT_POST, 'donaciones');
        
        if (is_numeric($dona) and $dona>0) {
            $p_key_token = get_option('pagoflash_key');
            $p_key_secret = get_option('pagoflash_secret');
            $p_url_punto_venta = urlencode(get_option('pagoflash_ptoventa'));
            $p_modo_prueba = (strtolower(get_option('pagoflash_testmode')) == 'on') ? true : false;
            $apiPF = new apiPagoflash($p_key_token, $p_key_secret, $p_url_punto_venta, $p_modo_prueba);

            $cabeceraDeCompra = array(
                "pc_order_number" => "donacion1", // Alfanumérico de máximo 45 caracteres.
                "pc_amount" => $dona // Float, sin separadores de miles, utilizamos el punto (.) como separadores de Decimales. Máximo dos decimales
            );

            $ProductItems = array();
            $product_1 = array(
                'pr_name' => 'Donación a Konkiro', // Nombre.  127 char max.
                'pr_desc' => ' Descripción del producto/servicio vendido.', // Descripción .  Maximo 230 caracteres.
                'pr_price' => $dona, // Precio individual. Float, sin separadores de miles, utilizamos el punto (.) como separadores de Decimales. Máximo dos decimales
                'pr_qty' => '1', // Cantidad, Entero sin separadores de miles  
                'pr_img' => 'http://konkiro.org/images/LOGO-KONKIRO-.png', // Dirección de imagen.  Debe ser una dirección (url) válida para la imagen.   
            );

            array_push($ProductItems, $product_1);

            $pagoFlashRequestData = array(
                'cabecera_de_compra' => $cabeceraDeCompra,
                'productos_items' => $ProductItems
            );

            $response = $apiPF->procesarPago($pagoFlashRequestData, $_SERVER['HTTP_USER_AGENT']);

            $pfResponse = json_decode($response);


            if ($pfResponse->success) {
                header("Location: " . $pfResponse->url_to_buy);
            } else {
                echo "ERROR<pre>";
                print_r(json_encode($pfResponse));
                echo "</pre>";
            }
        }        
    }

}


