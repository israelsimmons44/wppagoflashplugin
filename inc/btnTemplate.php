<form method="post" id="frmdonacion">
    <a href="#" id="urldonacion"><?php echo '<img src="' . $boton . '" />'; ?></a>
    <input type="hidden" name="donaciones" value="<?php echo $monto ?>">
    <?php wp_nonce_field('frm_donacion')?>
</form>

<script>
    jQuery(function(){
        jQuery('#urldonacion').on('click', function(e){
            e.preventDefault();
            jQuery('#frmdonacion').submit();
        });
    });
    
</script>