<?php
    $ptoVenta = get_option('pagoflash_ptoventa');
    $key = get_option('pagoflash_key');
    $secret = get_option('pagoflash_secret');
    $tokenError = get_option('pagoflash_tokenerror');
    $callback = get_option('pagoflash_callback');
    $testMode = (strtolower(get_option('pagoflash_testmode')) == 'on') ? 'checked' : '';
?>

<h2>WP PagoFlash Plugin</h2>
<div style="width:90%">
<div id="tabs">
    <ul>
        <li><a href="#tab1">Parámetros</a></li>
        <li><a href="#tab2">Información</a></li>
    </ul>
    <div id="tab1">
        <p class="code">Estos parámetros se encuentran en la información del Punto de Venta creado en el perfil de Pago Flash</p>
        <form class="frmPagoFlash" action ="options.php" method="post" name="options"> <?php echo wp_nonce_field('update-options') ?>
            <input type="hidden" name="action" value="update" /><input type="hidden" name="page_options" value="pagoflash_ptoventa,pagoflash_key,pagoflash_secret,pagoflash_testmode,pagoflash_tokenerror,pagoflash_callback" />
            <table class="form-table">
                <tr>
                    <th>Punto de Venta</th>
                    <td>
                        <input class="regular-text code" type="text" size="50" name="pagoflash_ptoventa" id="pagoflash_ptoventa" value="<?php echo $ptoVenta ?>"/>
                    </td>
                </tr>
                <tr>
                    <th>Key</th>
                    <td>
                        <input class="regular-text code" type="text"  size="50" name="pagoflash_key" id="pagoflash_key" value="<?php echo $key ?>"/>
                        <p class="description">Parametro Key del punto de vente de Pago Flash</p>
                    </td>
                </tr>
                <tr>
                    <th>Secret</th>
                    <td>
                        <input class="regular-text code" type="text" size="50" name="pagoflash_secret" id="pagoflash_secret" value="<?php echo $secret ?>"/>    
                        <p class="description">Parametro Secret del punto de vente de Pago Flash</p>
                    </td>
                </tr>
                <tr>
                    <th>Token de Error</th>
                    <td>
                        <input class="regular-text code" type="text" size="50" name="pagoflash_tokenerror" id="pagoflash_tokenerror" value="<?php echo $tokenError ?>"/>
                    </td>
                </tr>
                <tr>
                    <th>Página de respuesta</th>
                    <td>
                        <input class="regular-text code" type="text" size="50" name="pagoflash_callback" id="pagoflash_callback" value="<?php echo $callback ?>"/>
                        <p class="description">Lugar donde serán enviados los cliente luego del pago</p>
                    </td>
                </tr>
                <tr>
                    <th>Modo de Prueba</th>
                    <td>
                        <input type="checkbox" size="50" name="pagoflash_testmode" id="pagoflash_testmode" <?php echo $testMode ?> />Prueba
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        <?php echo submit_button()?>
                    </td>
                </tr>

            </table>


        </form>

    </div>
    <div id="tab2">
        <h2>Información de uso</h2>
        <p>Para poder utilizar el plugin debe tener un usuario registrado en <a href="http://www.pagoflash.com" target="_blank">Pago Flash</a> como empresa y haber creado un Punto de Venta</p>
        
        <h3>Formulario</h3>
        <p>Para hacer más fácil lainserción del formulario puedes utilizar el shortcode [frmPagoFlash]</p>
        
        <h3>Shortcodes</h3>
        <p>Para insertar el botón de pago flash se puede utilizar el shortcode [btnPagoFlash] en cualquier Post o Página<br/>
            El modificador size se emplea para modificar el tamaño del botón;<br> los valores pueden ser "1" o "2" como el siguiente ejemplo [btnPagoFlash size="2"]</p>
    </div>
</div>    
</div>


<script>
    jQuery(function(){
        jQuery('#tabs').tabs();
    })
    </script>
