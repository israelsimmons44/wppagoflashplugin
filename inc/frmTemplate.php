<form method="post">
    <label for="donaciones">Monto a donar:</label>
    <input type="text" name="donaciones" id="donaciones">
    <?php wp_nonce_field('frm_donacion')?>
    <a href="#" id="urldonacion"><?php echo '<img src="' . $boton . '" />'; ?></a>
    <button type="submit">Hacer Donativo</button>
</form>

<script>
    jQuery(function(){
        jQuery('#urldonacion').on('click', function(e){
            e.preventDefault();
            jQuery('#frmdonacion').submit();
        });
    });
    
</script>